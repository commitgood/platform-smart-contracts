const { accounts, contract, web3 } = require('@openzeppelin/test-environment');
const { TestHelper } = require('@openzeppelin/cli');
const { expectRevert } = require('@openzeppelin/test-helpers');
const { expect } = require('chai');
const { Contracts, ZWeb3 } = require('@openzeppelin/upgrades');

ZWeb3.initialize(web3.currentProvider);

const CommitGoodToken = Contracts.getFromLocal('CommitGoodToken');
const CommitGoodAccessControl = Contracts.getFromLocal('CommitGoodAccessControl');
const CommitGoodCampaign = Contracts.getFromLocal('CommitGoodCampaign');

describe('CommitGoodToken', function () {
    const [owner] = accounts;
    const capacity = 200000000;

    // Allows long running processes to complete otherwise you will get a timeout error - Andre
    this.timeout(0);

    beforeEach(async function () {
        this.project = await TestHelper();
        this.token = await this.project.createProxy(CommitGoodToken, {
            initMethod: 'initialize',
            initArgs: [owner, 'Commit Good', 'GOOD', 18, capacity]
        });
        this.accessControl = await this.project.createProxy(CommitGoodAccessControl, {
            initMethod: 'initialize',
            initArgs: [owner, owner]
        });
        await this.token.methods.setAccessControl(this.accessControl.address).send({ from: owner });

        this.campaign = await this.project.createProxy(CommitGoodCampaign, {
            initMethod: 'initialize',
            initArgs: [owner, this.token.address, this.accessControl.address, [1,2,3,4]]
        });
        await this.accessControl.whitelistCampaign(this.campaign.address);
    });

    it('ERC20 fields are set', async function () {
        expect(await this.token.methods.symbol().call()).to.equal('GOOD');
        expect(await this.token.methods.name().call()).to.equal('Commit Good');
        expect(await this.token.methods.decimals().call()).to.equal('18');
        expect(await this.token.methods.cap().call()).to.equal(capacity.toString());
        expect(await this.token.methods.owner().call()).to.equal(owner);
    });

    // it('validates donor for campaignApproval', async function () {
    //     const invalidDonor = accounts[1];
    //     const result = await this.token.methods.campaignApproval(invalidDonor, 123).send({ from: owner });
    //     expectRevert(result, 'The donor must be whitelisted.');
    // });

    // it('validates recipient for campaignApproval', async function () {
    //     expectRevert(await this.token.methods.campaignApproval(), 'Recipient must be a contract.');
    // });

    // it('validates contract for campaignApproval', async function () {
    //     expectRevert(await this.token.methods.campaignApproval(), 'Only the campaigns contract is allowed to execute campaignApproval.')
    // });
});