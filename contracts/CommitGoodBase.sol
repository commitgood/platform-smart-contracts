pragma solidity ^0.5.17;

contract CommitGoodBase {
    /**
     * @dev Emiited when a GSN enabled recipient processes a transaction.
     * @param actualCharge The estimated cost of the transaction.
     */
    event CommitGoodTransactionCharge(uint256 actualCharge);
}