pragma solidity ^0.5.17;

import "@openzeppelin/upgrades/contracts/Initializable.sol";
import "@openzeppelin/contracts-ethereum-package/contracts/ownership/Ownable.sol";
import "@openzeppelin/contracts-ethereum-package/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts-ethereum-package/contracts/token/ERC20/ERC20Detailed.sol";
import "@openzeppelin/contracts-ethereum-package/contracts/token/ERC20/ERC20Capped.sol";
import "@openzeppelin/contracts-ethereum-package/contracts/token/ERC20/ERC20Burnable.sol";
import "@openzeppelin/contracts-ethereum-package/contracts/token/ERC20/ERC20Pausable.sol";
import "@openzeppelin/contracts-ethereum-package/contracts/token/ERC20/ERC20Mintable.sol";
import "@openzeppelin/contracts-ethereum-package/contracts/utils/Address.sol";
import "./CommitGoodAccessControl.sol";
import "./CommitGoodBase.sol";

contract CommitGoodToken is
    Initializable,
    Ownable,
    ERC20,
    ERC20Detailed,
    ERC20Mintable,
    ERC20Capped,
    ERC20Burnable,
    ERC20Pausable,
    CommitGoodBase {
    CommitGoodAccessControl private _accessControl;

    function initialize(
        address owner,
        string memory name,
        string memory symbol,
        uint8 decimals,
        uint256 capacity) public initializer {
        Ownable.initialize(owner);
        ERC20Detailed.initialize(name, symbol, decimals);
        ERC20Capped.initialize(capacity, owner);
        ERC20Mintable.initialize(owner);
        ERC20Pausable.initialize(owner);
    }

    /**
     * @dev Allows the token owner to set the access control contract. In the future when the
     * organization can become an actual DAO, the owners can utilize a voting mechanism to
     * update the access control contract.
     * @param accessControl The address of the access control contract that adheres to the `CommitGoodAccessControl` contract.
     *
     * Requirements:
     *
     * - `msg.sender` must be the token owner.
     */
    function setAccessControl(CommitGoodAccessControl accessControl) public onlyOwner {
        _accessControl = accessControl;
    }

    /**
     * @dev Allows the campaigns contract to set approvals on the users behalf when executing donations.
     * @param donor The donor that executed the transaction.
     * @param amount The amount that is to be donated to the campaign.
     * @return A boolean value indicating that the approval succeeded.
     *
     * Requirements:
     *
     * - `donor` must be whitelisted.
     * - `_msgSender()` must be a contract and the campaigns contract.
     */
    function campaignApproval(address donor, uint256 amount) public returns(bool) {
        require(_accessControl.isUserWhitelisted(donor), "The donor must be whitelisted.");
        require(Address.isContract(_msgSender()), "Recipient must be a contract.");
        require(_accessControl.isCampaignWhitelisted(_msgSender()), "The campaign must be whitelisted.");
        _approve(donor, _msgSender(), amount);
        return true;
    }
}