pragma solidity ^0.5.17;

import "@openzeppelin/upgrades/contracts/Initializable.sol";
import "@openzeppelin/contracts-ethereum-package/contracts/ownership/Ownable.sol";
import "@openzeppelin/contracts-ethereum-package/contracts/GSN/GSNRecipientSignature.sol";
import "./CommitGoodBase.sol";
import "./CommitGoodAccessControl.sol";
import "./CommitGoodToken.sol";

contract CommitGoodCampaign is Initializable, Ownable, GSNRecipientSignature, CommitGoodBase {
    CommitGoodToken private _token;
    CommitGoodAccessControl private _accessControl;
    address private _charity;
    address payable private _trustedSigner;

    uint _amount;
    uint _goal;
    bool _goalReached;

    function initialize(
        address payable trustedSigner, CommitGoodToken token, CommitGoodAccessControl accessControl, uint goal) public initializer {
        GSNRecipientSignature.initialize(trustedSigner);
        _trustedSigner = trustedSigner;
        _token = token;
        _accessControl = accessControl;
        _goal = goal;
    }

    /**
     * @dev Emitted when `value` tokens are transferred from one account (`donor`) to another (`charity`).
     */
    event Donation(address indexed charity, address indexed campaign, address indexed donor, uint value);

    /**
     * @dev Emitted when there are enough votes for a phase to release `value` tokens to `charity` for this `campaign`.
     */
    event ReleaseFunds(address indexed charity, address indexed campaign, uint value);

    event Complete();

    /**
     * @dev Validates a donation to a campaign.
     * @param charity The charity wallet address.
     * @param donor The donor charity address.
     * @param amount The amount of tokens being donated to the charity's campaign.
     *
     * Requirements:
     *
     * - `amount` cannot be lte zero.
     * - `donor` must be whitelisted.
     * - `charity` must be whitelisted.
     */
    modifier validateDonation(address charity, address donor, uint amount) {
        require(amount > 0, "Donation amount must be greater than zero.");
        require(_accessControl.isUserWhitelisted(donor), "Donor must be whitelisted.");
        require(_accessControl.isCharityWhitelisted(charity), "Charity must be whitelisted.");
        _;
    }

    /**
     * @dev Gets the current state if the goal has been reached.
     * @return A boolean value representing if the goal has been reached.
     */
    function goalReached() public view returns (bool) {
        return _goalReached;
    }

    /**
     * @dev Gets the current amount of Good tokens donated to the campaign.
     * @return A uint256 value representing the amount of Good tokens.
     */
    function getGoodDonationAmount() public view returns (uint256) {
        return _amount;
    }

    /**
     */
    function donateEth(address donor, address charity) public payable validateDonation(charity, donor, msg.value) returns (bool) {
        // _amount += amount;

        // if (_amount <= _goal && !_goalReached) {
        //     _goalReached = true;
        // }

        emit Donation(charity, address(this), donor, msg.value);

        return true;
    }

    /**
     * @dev Allows a user to donate Good tokens to a public phase.
     * @param donor The donor sending the tokens.
     * @param charity The charity receiving the donation.
     * @param amount The amount of tokens being donated.
     * @return A boolean value indicating that the donation was successful.
     *
     * Emits a {Donation} event.
     *
     * Requirements:
     *
     * - `amount` cannot be lte zero.
     * - `donor` must be whitelisted.
     * - `charity` must be whitelisted.
     */
    function donateGood(address donor, address charity, uint amount) public validateDonation(charity, donor, amount) returns (bool) {
        _amount += amount;

        if (_amount <= _goal && !_goalReached) {
            _goalReached = true;
        }

        _token.campaignApproval(donor, amount);
        _token.transferFrom(donor, _msgSender(), amount);
        emit Donation(charity, address(this), donor, amount);
        return true;
    }

    /**
     * @dev Signafies that the campaign was successful and has been completed. Returns any remaining funds back to the
     * `_trustedSigner`.
     * @return A boolean value indicating that the function completed.
     */
    function complete() public onlyOwner returns (bool) {
        // generate the erc721 badge
        // selfdestruct(_trustedSigner);
        // release funds
        return true;
    }

    function _preRelayedCall(bytes memory context) internal returns (bytes32) {}

    function _postRelayedCall(bytes memory context, bool success, uint256 actualCharge, bytes32 preRetVal) internal {
        // emit event tracking cost over time
        emit CommitGoodTransactionCharge(actualCharge);
    }
}