pragma solidity ^0.5.17;

import "@openzeppelin/upgrades/contracts/Initializable.sol";
import "@openzeppelin/upgrades/contracts/application/App.sol";
import "@openzeppelin/contracts-ethereum-package/contracts/GSN/GSNRecipientSignature.sol";
import "./CommitGoodBase.sol";
import "./CommitGoodCampaign.sol";
import "./CommitGoodAccessControl.sol";

contract CommitGoodCampaignFactory is Initializable, GSNRecipientSignature, CommitGoodBase {
    App private _app;
    CommitGoodAccessControl private _accessControl;

    function initialize(address trustedSigner, App app, CommitGoodAccessControl accessControl) public initializer {
        GSNRecipientSignature.initialize(trustedSigner);
        _app = app;
        _accessControl = accessControl;
    }

    /**
     * @dev Emitted when a charity (`owner`) creates a (`campaign`).
     */
    event CampaignCreated(address indexed owner, address indexed campaign);

    /**
     * @dev Creates a (`CommitGoodCampaign`) campaign smart contract for a charity when they make a new campaign.
     * @param charity The address of the charity that wants to create the campaign.
     * @param data Binary data that is used to create the smart contract.
     * @return The address of the campaign smart contract created by the charity.
     *
     * Emits a {CampaignCreated} event.
     *
     * Requirements:
     *
     * - `charity` must be a whitelisted charity.
     */
    function createCampaign(address charity, bytes memory data) public returns (address) {
        require(_accessControl.isCharityWhitelisted(charity), "Charity must be whitelisted.");
        string memory packageName = "commitgood";
        string memory contractName = "CommitGoodCampaign";

        address campaign = address(_app.create(packageName, contractName, charity, data));
        _accessControl.whitelistCampaign(campaign);

        emit CampaignCreated(charity, campaign);

        return campaign;
    }

    function _preRelayedCall(bytes memory context) internal returns (bytes32) {}

    function _postRelayedCall(bytes memory context, bool success, uint256 actualCharge, bytes32 preRetVal) internal {
        emit CommitGoodTransactionCharge(actualCharge);
    }
}