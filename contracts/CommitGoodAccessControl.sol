pragma solidity ^0.5.17;

import "@openzeppelin/upgrades/contracts/Initializable.sol";
import "@openzeppelin/contracts-ethereum-package/contracts/GSN/GSNRecipientSignature.sol";
import "@openzeppelin/contracts-ethereum-package/contracts/ownership/Ownable.sol";
import "@openzeppelin/contracts-ethereum-package/contracts/utils/Address.sol";
import "./CommitGoodBase.sol";

/**
 * @dev Contract that sets access control levels for users and charities.
 */
contract CommitGoodAccessControl is Initializable, Ownable, GSNRecipientSignature, CommitGoodBase {
    mapping(address => bool) public users;
    mapping(address => bool) public charities;
    mapping(address => bool) public campaigns;

    function initialize(address owner, address trustedSigner) public initializer {
        GSNRecipientSignature.initialize(trustedSigner);
        Ownable.initialize(owner);
    }

    /**
     * @dev Roles that are available in the system.
     */
    enum Roles { Charity, User, Mod }

    /**
     * @dev Emitted when `registrar` has their authorization `enabled`.
     * @param registrar Address of the account to be authorized or deauthorized.
     * @param role Type of `Roles` either `User` or `Charity`.
     * @param enabled Boolean value that denotes the authorization.
     */
    event Authorize(address indexed registrar, Roles role, bool enabled);

    /**
     * @dev Emitted when a charity creates or copmletes a campaign.
     * @param campaign Address of the campaign contract.
     * @param enabled Boolean value that denotes the authorization.
     */
    event CampaignAuthorize(address indexed campaign, bool enabled);

    /**
     * @dev Validates an address
     * @param account The address that is being validated
     *
     * Requirements:
     *
     * - `account` cannot be a zero address or a contract address.
     */
    modifier validAddress(address account) {
        require(account != address(0), "0x0 is not a valid address");
        require(!Address.isContract(account), "Contract address is not a valid address");
        _;
    }

    /**
     * @dev Validates a contract address
     * @param kontract The address that is being validated
     *
     * Requirements:
     *
     * - `kontract` cannot be a zero address and must be a contract address.
     */
    modifier validContract(address kontract) {
        require(kontract != address(0), "0x0 is not a valid address");
        require(Address.isContract(kontract), "Contract address is not a valid address");
        _;
    }

    /**
     * @dev Adds a user to the whitelist.
     * @param account The wallet address of the user.
     * @return True indicating the function completed successfully.
     *
     * Emits a {Authorize} event.
     *
     * Requirements:
     *
     * - `account` cannot be a zero address and must not be a contract.
     */
    function whitelistUser(address account) public validAddress(account) returns (bool) {
        users[account] = true;
        emit Authorize(account, Roles.User, true);
        return true;
    }

    /**
     * @dev Removes a user from the whitelist.
     * @param account The wallet address of the user.
     * @return True indicating the function completed successfully.
     *
     * Emits a {Authorize} event.
     *
     * Requirements:
     *
     * - `account` cannot be a zero address and must not be a contract.
     */
    function blacklistUser(address account) public onlyOwner validAddress(account) returns (bool) {
        users[account] = false;
        emit Authorize(account, Roles.User, false);
        return true;
    }

    /**
     * @dev Adds a campaign from the whitelist.
     * @param campaign The contract address of the campaign.
     * @return True indicating the function completed successfully.
     *
     * Emits a {CampaignAuthorize} event.
     *
     * Requirements:
     *
     * - `campaign` cannot be a zero address and must be a contract.
     */
    function whitelistCampaign(address campaign) public validContract(campaign) returns (bool) {
        campaigns[campaign] = true;
        emit CampaignAuthorize(campaign, true);
        return true;
    }

    /**
     * @dev Removes a campaign from the whitelist.
     * @param campaign The contract address of the campaign.
     * @return True indicating the function completed successfully.
     *
     * Emits a {CampaignAuthorize} event.
     *
     * Requirements:
     *
     * - `campaign` cannot be a zero address and must be a contract.
     */
    function blacklistCampaign(address campaign) public validContract(campaign) returns (bool) {
        campaigns[campaign] = false;
        emit CampaignAuthorize(campaign, false);
        return true;
    }

    /**
     * @dev Sets the authorization status of a charity.
     * @param account The wallet address of the account.
     * @param enable The authorization status of a charity.
     * @return True indicating the function completed successfully.
     *
     * Emits a {Authorize} event.
     *
     * Requirements:
     *
     * - `account` cannot be a zero address.
     */
    function authorizeCharity(address account, bool enable) public onlyOwner validAddress(account) returns (bool) {
        charities[account] = enable;
        emit Authorize(account, Roles.Charity, enable);
        return true;
    }

    /**
     * @dev Returns a boolean value indicating if the user is whitelisted.
     * @param account The wallet address of the user.
     * @return A boolean value indicating if the user is whitelisted.
     *
     * Requirements:
     *
     * - `account` cannot be a zero address.
     */
    function isUserWhitelisted(address account) public view validAddress(account) returns (bool) {
        return users[account];
    }

    /**
     * @dev Returns a boolean value indicating if the charity is whitelisted.
     * @param account The wallet address of the charity.
     * @return A boolean value indicating if the charity is whitelisted.
     *
     * Requirements:
     *
     * - `account` cannot be a zero address.
     */
    function isCharityWhitelisted(address account) public view validAddress(account) returns (bool) {
        return charities[account];
    }

    /**
     * @dev Returns a boolean value indicating if the campaign is whitelisted.
     * @param campaign The contract address of the campaign.
     * @return A boolean value indicating if the campaign is whitelisted.
     *
     * Requirements:
     *
     * - `campaign` cannot be a zero address and must be a contract.
     */
    function isCampaignWhitelisted(address campaign) public view validContract(campaign) returns (bool) {
        return campaigns[campaign];
    }

    function _preRelayedCall(bytes memory context) internal returns (bytes32) {}

    function _postRelayedCall(bytes memory context, bool success, uint256 actualCharge, bytes32 preRetVal) internal {
        emit CommitGoodTransactionCharge(actualCharge);
    }
}