pragma solidity ^0.5.17;

import "@openzeppelin/upgrades/contracts/Initializable.sol";
import "@openzeppelin/contracts-ethereum-package/contracts/GSN/GSNRecipientSignature.sol";
import "./CommitGoodBase.sol";

contract CommitGoodPlatform is Initializable, GSNRecipientSignature, CommitGoodBase {
    function initialize(address trustedSigner) public initializer {
        GSNRecipientSignature.initialize(trustedSigner);
    }
    
    /**
     */
    function awardCoordinatorForSignups() public returns (bool) {
        return true;
    }

    /**
     */
    function awardCoordinatorForSuccessfulProjects() public returns (bool) {
        return true;
    }

    function _preRelayedCall(bytes memory context) internal returns (bytes32) {}

    function _postRelayedCall(bytes memory context, bool, uint256 actualCharge, bytes32) internal {
        // emit event tracking cost over time
        emit CommitGoodTransactionCharge(actualCharge);
    }
}