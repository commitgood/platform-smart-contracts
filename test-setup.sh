ganache_url="http://localhost:8545"
relayer_port=8099
relayer_url="http://localhost:${relayer_port}"

relayer_running() {
  nc -z localhost "$relayer_port"
}

setup_gsn_relay() {
  relay_hub_addr=$(npx oz-gsn deploy-relay-hub --ethereumNodeURL $ganache_url)
  echo "Launching GSN relay server to hub $relay_hub_addr"

  ./bin/gsn-relay -DevMode -RelayHubAddress $relay_hub_addr -EthereumNodeUrl $ganache_url -Url $relayer_url &> /dev/null &
  gsn_relay_server_pid=$!

  while ! relayer_running; do
    sleep 0.1
  done
  echo "GSN relay server launched!"

  npx oz-gsn register-relayer --ethereumNodeURL $ganache_url --relayUrl $relayer_url
}